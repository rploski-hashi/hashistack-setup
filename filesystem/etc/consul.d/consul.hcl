datacenter = "dc1"
data_dir = "/opt/consul"
encrypt = "YOUR_CONSUL_ENCRYPTION_KEY"
ca_file = "/etc/consul.d/consul-agent-ca.pem"
verify_outgoing = true
verify_server_hostname = true
retry_join = ["10.0.1.211","10.0.1.212","10.0.1.215"]
auto_encrypt {
  tls = true
}
log_level = "INFO"
