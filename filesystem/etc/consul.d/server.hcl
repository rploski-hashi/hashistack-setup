# Remove this file from all Consul cluster nodes except those that will perform the part as a 
# Consul server.
server = true
bootstrap_expect = 3
ui = true
client_addr = "0.0.0.0"
verify_incoming = true
cert_file = "/etc/consul.d/dc1-server-consul-0.pem"
key_file = "/etc/consul.d/dc1-server-consul-0-key.pem"
auto_encrpyt = {
    allow_tls = true
}