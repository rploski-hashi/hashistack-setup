#!/bin/bash
# A simple script to get the latest versions of HashiCorp tech and do the initial configs.
# Currently only tested upon Ubuntu 20 LTS.


CONSUL_VERSION="1.8.0"
NOMAD_VERSION="0.12.0"
VAULT_VERSION="1.4.3"

apt install unzip -y

#Set up Consul
wget -N https://releases.hashicorp.com/consul/${CONSUL_VERSION}/consul_${CONSUL_VERSION}_linux_arm64.zip
unzip consul*
mv ./consul /usr/local/bin/.
consul -autocomplete-install
complete -C /usr/local/bin/consul consul
mkdir /etc/consul.d
chown --recursive consul:consul /etc/consul.d
useradd --system --home /etc/consul.d --shell /bin/false consul
mkdir --parents /opt/consul
chown --recursive consul:consul /opt/consul
chmod 700  /etc/consul.d

#Set up Nomad
wget -N https://releases.hashicorp.com/nomad/${NOMAD_VERSION}/nomad_${NOMAD_VERSION}_linux_arm64.zip
unzip nomad*
chown root:root nomad
mv ./nomad /usr/local/bin/.
nomad -autocomplete-install
complete -C /usr/local/bin/nomad nomad
mkdir /etc/nomad.d
useradd --system --home /etc/nomad.d --shel /bin/false nomad
mkdir --parents /opt/nomad
chown --recursive nomad:nomad /opt/nomad
chmod 700 /etc/nomad./d

# Set up Vault
wget -N https://releases.hashicorp.com/vault/${VAULT_VERSION}/vault_${VAULT_VERSION}_linux_arm64.zip
unzip vault*
chown root:root vault
mv ./vault /usr/local/bin/.
vault -autocomplete-install
complete -C /usr/local/bin/vault vault

# Give Vault the ability to use the mlock syscall w/o being root. (prohibits mem to be written to disk)
setcap cap_ipc_lock=+ep /usr/local/bin/vault
mkdir /etc/vault.d
useradd --system --home /etc/vault.d --shell /bin/false vault
chmod 700 /etc/vault.d
