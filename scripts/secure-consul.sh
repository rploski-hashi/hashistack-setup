#!/bin/bash
# A script that generates the gossip encryption key, and TLS certs for RPC encryption

# Default datacenter is named dc1
# TODO:  Make this an argument input.
DC="dc1"


# generate the encryption key needed for the entire cluster
CONSUL_KEYGEN=`consul keygen`
echo "Congratulations, your are one step closer to securing your Consul cluster!.  \
Your encryption key will be needed for any of your agents to communicate with the 
cluster.  Please story your encryption key in a safe place."
echo "Your CONSUL ENCRYPTION KEY IS:$CONSUL_KEYGEN"

# generate the TLS certificate .pem client files
consul tls ca create

# generate the TLS certificate .pem server files
consul tls cert create -server -dc $DC

echo "Consul client and server certificates should have been generated in this directory.\
You should execute the following command to copy the client certficate to all members of the cluster:"
echo "$ scp consul-agent.pem $DC-client-consul-0.pem $DC-client-consul-0-key.pem YOUR_USERNAME@YOUR_CONSUL_IP:/etc/consul.d/"
echo "For server nodes, use the following command:"
echo "$ scp consul-agent.pem $DC-server-consul-0.pem $DC-server-consul-0.key.pem YOUR_USERNAME@YOUR_SERVER_IP:/etc/consul.d/"
echo "NOTE:  You should have run get-hashistack.sh on those other servers already - do so before executing these scp commands."