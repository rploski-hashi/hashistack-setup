#!/bin/bash

# Set up the servers to have write privileges
consul acl policy create -name consul-server-callisto -rules @consul-callisto.hcl
consul acl policy create -name consul-server-ganymede -rules @consul-ganymede.hcl
consul acl policy create -name consul-server-io -rules @consul-io.hcl

# Do the same with the clients - provide them write privs to their node.
consul acl policy create -name consul-client-saturn -rules @consul-saturn.hcl
consul acl policy create -name consul-client-europa -rules @consul-europa.hcl
consul acl policy create -name consul-client-enceladus -rules @consul-enceladus.hcl
consul acl policy create -name consul-client-phobos -rules @consul-phobos.hcl
consul acl policy create -name consul-client-jupiter -rules @consul-jupiter.hcl
